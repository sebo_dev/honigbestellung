import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import reportWebVitals from './reportWebVitals';

class Warenkorb extends React.Component{
  constructor(props){
    super(props);
    this.state={
      h1: 0,
      h2: 0,
      h3: 0,
      h4: 0,
    };

    this.handleChange = this.handleChange.bind(this);
  }
  handleClick(){
    this.props.onClick(this.state.h1,this.state.h2,this.state.h3,this.state.h4,)
  }
  handleChange(event){

    switch(event.target.id){
      case 'h1':
        this.setState({h1: event.target.value});
        break;
      case 'h2':
        this.setState({h2: event.target.value});
        break;
      case 'h3':
        this.setState({h3: event.target.value});
        break;
      case 'h4':
        this.setState({h4: event.target.value});
        break;
    }
  }

  render(){

    return(
      <div>
        <table>
          <tr>
            <th>Honig</th>
            <th>Menge</th>
          </tr>
          <tr>
            <td>Akazienhonig</td>
            <td><input id="h1" type="textbox" value={this.state.h1} onChange={this.handleChange}></input></td>
          </tr>
          <tr>
            <td>Heidehonig</td>
            <td><input id="h2" type="textbox" value={this.state.h2} onChange={this.handleChange}></input></td>
          </tr>
          <tr>
            <td>Kleehonig</td>
            <td><input id="h3" type="textbox" value={this.state.h3} onChange={this.handleChange}></input></td>
          </tr>
          <tr>
            <td>Tannenhonig</td>
            <td><input id="h4" type="textbox" value={this.state.h4} onChange={this.handleChange}></input></td>
          </tr>
          <tr>
            <td colSpan="2"><button onClick={()=>this.handleClick()}>Abschicken</button></td>
          </tr>
        
        </table>
      </div>
    )
  }
}

class PersDaten extends React.Component{
  constructor(props){
    super(props);
    this.state={
      vname: "",
      nname: "",
      wohnort: "",
      mailadresse: "",
    };

    this.handleChange = this.handleChange.bind(this);
  }
  handleClick(){
    this.props.onClick(this.state.vname,this.state.nname,this.state.wohnort,this.state.mailadresse,)
  }
  handleChange(event){

    switch(event.target.id){
      case 'vname':
        this.setState({vname: event.target.value});
        break;
      case 'nname':
        this.setState({nname: event.target.value});
        break;
      case 'wohnort':
        this.setState({wohnort: event.target.value});
        break;
      case 'mailadresse':
        this.setState({mailadresse: event.target.value});
        break;
    }
  }

  render(){

    return(
      <div>
        <table>
          <tr>
            <th>Honig</th>
            <th>Menge</th>
          </tr>
          <tr>
            <td>Vorname</td>
            <td><input id="vname" type="textbox" value={this.state.vname} onChange={this.handleChange}></input></td>
          </tr>
          <tr>
            <td>Nachname</td>
            <td><input id="nname" type="textbox" value={this.state.nname} onChange={this.handleChange}></input></td>
          </tr>
          <tr>
            <td>Lieferadresse</td>
            <td><input id="wohnort" type="textbox" value={this.state.wohnort} onChange={this.handleChange}></input></td>
          </tr>
          <tr>
            <td>Mailadresse</td>
            <td><input id="mailadresse" type="textbox" value={this.state.mailadresse} onChange={this.handleChange}></input></td>
          </tr>
          <tr>
            <td colSpan="2"><button onClick={()=>this.handleClick()}>Abschicken</button></td>
          </tr>
        
        </table>
      </div>
    )
  }
}

class Bestellung extends React.Component {
  constructor(props){
    super(props);
    this.state={
      fortschritt: 0,
      h1: 0,
      h2: 0,
      h3: 0,
      h4: 0,
      vname: 0,
      nname: 0,
      wohnort: 0,
      mailadresse: 0,
    };

    this.handleAbschicken = this.handleAbschicken.bind(this);
    this.handleDatenAbschicken = this.handleDatenAbschicken.bind(this);
    this.handleWeiter = this.handleWeiter.bind(this);
    this.handleNeu = this.handleNeu.bind(this);

  }

  handleAbschicken(h1v,h2v,h3v,h4v){
    this.setState({fortschritt: 1,h1: h1v, h2: h2v, h3: h3v, h4: h4v});
  }
  handleDatenAbschicken(vnamev,nnamev,wohnortv,mailadressev){
    this.setState({fortschritt: 3,vname: vnamev, nname: nnamev, wohnort: wohnortv, mailadresse: mailadressev});
  }

  handleWeiter(){
    this.setState({fortschritt: 2});
  }

  handleNeu(){
    this.setState({fortschritt: 0,h1: 0, h2: 0, h3: 0, h4: 0,vname: "", nname: "", wohnort: "", mailadresse: ""});
  }

  render(){

    if(this.state.fortschritt==0){
      return(
        <div>
          <Warenkorb onClick={this.handleAbschicken}/>
        </div>
      )
    }
    else if(this.state.fortschritt==1){
      return(
        <div>
            <p>Akazienholz: {this.state.h1}</p>
            <p>Heidehonig: {this.state.h2}</p>
            <p>Kleehonig: {this.state.h3}</p>
            <p>Tannenhonig: {this.state.h4}</p>
            <button onClick={this.handleWeiter}>Weiter</button>
        </div>
      )
    }
    else if(this.state.fortschritt==2){
      return(
        <div>
          <PersDaten onClick={this.handleDatenAbschicken}/>
        </div>
      )
    }
    else if(this.state.fortschritt==3){
      return(
        <div>
            <p>Akazienholz: {this.state.h1}</p>
            <p>Heidehonig: {this.state.h2}</p>
            <p>Kleehonig: {this.state.h3}</p>
            <p>Tannenhonig: {this.state.h4}</p>
            <p>Vorname: {this.state.vname}</p>
            <p>Nachname: {this.state.nname}</p>
            <p>Lieferadresse: {this.state.wohnort}</p>
            <p>Mailadresse: {this.state.mailadresse}</p>
            <button onClick={this.handleNeu}>Neu beginnen</button>
        </div>
      )
    }
    else{
      return(
        <div><p>Something is wrong here</p></div>
      )
    }

    
  }
}

ReactDOM.render(

    <Bestellung />,
    document.getElementById('root')

);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
